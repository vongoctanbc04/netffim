import * as React from 'react';
import Phana from './phana';
import Phanb from './phanb';
import Phanc from './phanc';

export interface IAboutProps {
}

export default function About (props: IAboutProps) {
  return (
    <div>
      <Phana />
      <Phanb/>
      <Phanc />
    </div>
  );
}
