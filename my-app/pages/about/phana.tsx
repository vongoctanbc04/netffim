import Image from 'next/image';
import * as React from 'react';
import img from "../image/img.jpeg"
import bien from "../image/bien.png"
import hinh from "../image/hinh.png"
import nui from "../image/nui.png"


export interface IPhanaProps {
}

export default function Phana (props: IPhanaProps) {
  return (
    <div>
<nav className="text-right pr-40">
  <a className="text-blue-900 p-5 hover:text-red-500" href="https://www.youtube.com/">Home</a>
  <a className="text-blue-900 hover:text-red-500" href="#">Works</a>
  <a className="text-blue-900 p-5  hover:text-red-500" href="#">Blog</a>
</nav>
<div className='pl-40 pt-20 flex'>
    <div className='text-left'>
      <h1 className=' text-3xl'>Hi, I am John,</h1>
      <h1 className=' text-3xl font-bold'>Creative Technologist</h1>
      <p className="pt-5">I can't get you off my mind 
I just don't know  what to do to have<br></br> you back here again
I can't let go, can't let you god 
I just don't know  what to do to have<br></br> I can't get you off my mind 
I just don't know  what to do to have.?
</p>
<button className="text-white bg-red-400 p-2 mt-5 rounded-xl hover:bg-black">DOWNLOAD RESUME</button>
    </div>
    <div><Image className='rounded-full w-50' src={img}  alt = "avatar"></Image></div>
 
</div>
<div className="bg-slate-200 max-h-96 mt-10  pl-40">
  <h2 className="text-left py-5 font-bold">Recent Post</h2>
  <div className='flex bg-slate-200'>
    <div className="bg-white text-left my-5 mx-5">
      <h1 className="p-3 font-bold">Making a design system from </h1>
      <span className="pl-3 text-red-900">12 Feb-2020</span> 
      <p className="p-3">Google development. It provides website interfaces,<br></br>and application programming interfaces that<br></br> build web browser extensions and software applications.</p>
    </div>
    <div className="bg-white text-left my-5 mx-5">
      <h1 className="p-3 font-bold">Making a design system from </h1>
      <span className="pl-3 text-red-900">12 Feb-2020</span>
      <p className="p-3">Google development. It provides website interfaces,<br></br>and application programming interfaces that<br></br> build web browser extensions and software applications.</p>
    </div>
  </div>
</div>
<div className="pl-40">
<h2 className="text-left py-5 font-bold">Featured works</h2>

<div className='flex pt-10'>
  <div className="pr-10">
    <Image src={bien} alt = "avatar" height={200} ></Image>
  </div>
  <div>
    <h1 className="text-2xl text-red-900 pb-5">Designing Dashboards</h1>
    <button className="rounded-md bg-slate-300 px-3">2020</button>
    <p className='pt-5 hover:text-red-900'>Hi admin, I have a question. On google translate,<br></br> when I finished translating, I clicked back to the sentence <br></br>I translated before, but it kept going back to each word,<br></br> not the whole sentence.</p>
  </div>

</div>
<div className='flex pt-10'>
  <div className="pr-10">
    <Image src={nui} alt = "avatar" height={200} ></Image>
  </div>
  <div>
    <h1 className="text-2xl text-red-900 pb-5">Designing Dashboards</h1>
    <button className="rounded-md bg-slate-300 px-3">2020</button>
    <p className='pt-5 hover:text-red-900'>Hi admin, I have a question. On google translate,<br></br> when I finished translating, I clicked back to the sentence <br></br>I translated before, but it kept going back to each word,<br></br> not the whole sentence.</p>
  </div>

</div>

</div>

    </div>
  );
}
