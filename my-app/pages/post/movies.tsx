
import Link from 'next/link';
import React, { useEffect, useState } from 'react';



export default function Movies () {
  const [movies,setMovies]= useState([])
  useEffect(()=>{
    (async ()=>{
      const response = await fetch("https://62db6dffe56f6d82a7729563.mockapi.io/phone")
      const data = await response.json()
      setMovies(data)
      console.log(movies)

    })()
  },[])

  return (
    <div className="text-white bg-black">
      <h1 className="pt-10 pl-10 text-lime-700">Trending Now</h1>
      <div className='grid grid-cols-4 gap-4'>
        {movies.map((a:any)=><div key={a.id} className="">
        <div className="text-center " style={{width: '18rem'}}>
  <img src={a.img} className="card-img-top" alt="..." />
  <div className="card-body">
    <h5 className="card-title">{a.name}</h5>
    <p className="font-thin">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <Link href="/about"> <button className="bg-cyan-500 shadow-lg shadow-cyan-500/50 px-10 py-1 rounded-xl hover:bg-amber-300">Chi Tiết</button></Link>
   
  </div>
</div>

        </div>)}
      </div>
    </div>
  );
}
