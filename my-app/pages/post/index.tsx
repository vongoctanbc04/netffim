import * as React from 'react';
import Header from './header';
import Movies from './movies';

export interface IPostProps {
}

export default function Post (props: IPostProps) {
  return (
    <div>
      < Header/>
      <Movies/>
    </div>
  );
}
