import { url } from 'inspector';
import Image from 'next/image';
import * as React from 'react';
import next from "../image/next.png"
import sup from "../image/sup.png"
export interface IHeaderProps {
}

export default function Header (props: IHeaderProps) {
  return (
    <div>
      <div className='bg-black'>
        <div className='flex'>
            <div>
                <a className="text-red-600 text-6xl font-black hover:text-white" href="">NETFLIX</a>  
            </div>
            <div className=' text-2xl pt-5 pl-20 text-white'>
                <a className='hover:text-amber-400' href="https://www.netflix.com/vn/">Home</a>
                <a className='pl-10 hover:text-amber-400'href="#">TV Shows</a>
                <a className='pl-10 hover:text-amber-400' href="#">Movies</a>
                <a className='pl-10 hover:text-amber-400'href="#">New & Poplar</a>
                <a className='pl-10 hover:text-amber-400'href="#">My List</a>
            </div>
        </div>
        <div></div>
      </div>
      <div >
      {/* style={{ backgroundImage: 'url(https://picsum.photos/id/1/300/600)'}} */}
      <Image className="absolute" src={sup} alt="" style={{width: "100%"}}/>
      <div className="relative text-white pt-20 pl-20">
        <h1 className='text-5xl font-bold'>No Exit</h1>
        <p className='pt-10 font-thin pb-20'>Stranded at a rest stop in the mountains during a blizzard ,a <br></br>
        recovering addict discover a kidnapped child hidden in a car <br></br>
        belonging to one of the people inside the building which sets her <br></br>
        on a terrifying struggle to identify who among them is the <br></br> kidnapper.!</p>
        <button className="text-red-500 px-10 py-2 bg-orange-200 rounded-md hover:bg-slate-700">Play</button>
        <button className="ml-20 text-red-500 px-10 py-2 bg-cyan-500 shadow-lg shadow-cyan-500/50 rounded-md hover:bg-slate-700">More info</button>
        <p className="pt-40">Trending Now</p>
      </div>
      </div>
    </div>
  );
}
