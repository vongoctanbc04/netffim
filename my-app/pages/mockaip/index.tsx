import  React, { useEffect, useState } from 'react';

let mangSv = []
 
function showThongTinLenFrom(sv){
    
    let contentHTML = "";
    for(var a = 0; a < sv.length; a ++){
        var index = sv[a]
        var trContent = ` <tr>
    <td >${index.name}</td>
    <td>${index.id}</td>
    <td>${index.ngay}</td>
    <td>${index.diem}</td>
    <td>${index.xepl}</td>
    <td>${index.hanhk}</td>
    <td>
    <button>Xoá</button>
    <button>Sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent
    }
    document.getElementById("tbodySinhVien").innerHTML= contentHTML;
}
export default function Mockaip () {
    const [sv,setSv] = useState([])
useEffect(()=>{
    (async()=>{
        const response = await fetch("https://62db6dffe56f6d82a7729563.mockapi.io/sv")
        const data = await response.json()
// console.log(data);

        setSv(data)
        // console.log("sv",sv);
 // console.log(sv)
        mangSv =sv
        showThongTinLenFrom(mangSv)
       
    })()
},[])

  return (
    <div>
              <div>
        <h1 className="text-center text-2xl text-emerald-400">Thông Tin Sinh Viên</h1>

   <label>Tên sinh viên: </label>

<input  type="" className=" border border-black" placeholder="Tên sinh viên" id='name'  />
<label>Mã sinh viên: </label>
<input  type="" className=" border border-black" placeholder="Mã sinh viên" id='id'  />
<label>Ngày sinh: </label>
<input  type="" className=" border border-black" placeholder="Ngày sinh" id='style'  />
<label>Diem trung bình: </label>
<input  type="" className=" border border-black" placeholder="Diem trung bình" id='diem'  />
<p className='px-5'></p>
<label>Xep loai: </label>
<input  type="" className=" border border-black" placeholder="Xep loai" id='loai'  />
<label>Hạnh kiểm: </label>
<input  type="" className=" border border-black" placeholder="Hạnh kiểm" id='hk'  />
<p></p>
             </div>
             <div  className="text-center">
                 <button className="bg-lime-200 p-2 rounded-3xl hover:bg-red-600">Thêm Sinh Viên</button>
             </div>
             <h1 className="text-center text-2xl text-emerald-400">DANH SÁCH SINH VIEN</h1>

<table className="border-collapse border border-slate-400 ">
  <thead >
    <tr>
      <th className="border border-slate-300 ">Họ Tên SV</th>
      <th className="border border-slate-300 ">Mã Sinh Viên</th>
      <th className="border border-slate-300 "> Ngày Sinh </th>
      <th className="border border-slate-300 ">Điểm Trung Bình</th>
      <th className="border border-slate-300 ">Xếp Loại Học Lực</th>
      <th className="border border-slate-300 ">Hạnh kiểm</th>
      <th className="border border-slate-300 ">Chức Năng</th>
    
    </tr>
  </thead>
  <tbody id="tbodySinhVien"  className="text-center border border-slate-300"></tbody>
</table>
    </div>
  );
}

