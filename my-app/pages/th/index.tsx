import  React from 'react';


let mangSV= []

function layThongTinTuFrom(){
    let a = document.getElementById("name").value;
    let b = document.getElementById("id").value;
    let c = document.getElementById("style").value;
    let d = document.getElementById("diem").value;
    let e = document.getElementById("loai").value;
    let g = document.getElementById("hk").value;


    let sv ={
      ten:a,
      ma:b,
      ngay: c,
      diem: d,
      loai: e,
      hanh: g,
    }
    console.log(sv)
    return sv    
} 

function showThongTinLenFrom(mangSV){
  let contentHTML = "";
  for(var a = 0; a < mangSV.length; a ++){
    var sv = mangSV[a]
    var trContent = ` <tr>
    <td >${sv.ten}</td>
    <td>${sv.ma}</td>
    <td>${sv.ngay}</td>
    <td>${sv.diem}</td>
    <td>${sv.loai}</td>
    <td>${sv.hanh}</td>
    <td>
    <button onClick={onC} style={{color:red}}>Xoá</button>
    <button>Sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent
  }
 
document.getElementById("tbodySinhVien").innerHTML= contentHTML;

}
function inRa(){
  let sv = layThongTinTuFrom()
  mangSV.push(sv)
  showThongTinLenFrom(mangSV)
  console.log(mangSV)
}

export default function Th () {
  return (
    
    <div>
        <div>
        <h1 className="text-center text-2xl text-emerald-400">Thông Tin Sinh Viên</h1>

   <label>Tên sinh viên: </label>

<input  type="" className=" border border-black" placeholder="Tên sinh viên" id='name'  />
<label>Mã sinh viên: </label>
<input  type="" className=" border border-black" placeholder="Mã sinh viên" id='id'  />
<label>Ngày sinh: </label>
<input  type="" className=" border border-black" placeholder="Ngày sinh" id='style'  />
<label>Diem trung bình: </label>
<input  type="" className=" border border-black" placeholder="Diem trung bình" id='diem'  />
<p className='px-5'></p>
<label>Xep loai: </label>
<input  type="" className=" border border-black" placeholder="Xep loai" id='loai'  />
<label>Hạnh kiểm: </label>
<input  type="" className=" border border-black" placeholder="Hạnh kiểm" id='hk'  />
<p></p>
        </div>

<div  className="text-center">
<button onClick={inRa} className="bg-lime-200 p-2 rounded-3xl hover:bg-red-600">Thêm Sinh Viên</button>
</div>

        <h1 className="text-center text-2xl text-emerald-400">DANH SÁCH SINH VIEN</h1>

<table className="border-collapse border border-slate-400 ">
  <thead >
    <tr>
      <th className="border border-slate-300 ">Họ Tên SV</th>
      <th className="border border-slate-300 ">Mã Sinh Viên</th>
      <th className="border border-slate-300 "> Ngày Sinh </th>
      <th className="border border-slate-300 ">Điểm Trung Bình</th>
      <th className="border border-slate-300 ">Xếp Loại Học Lực</th>
      <th className="border border-slate-300 ">Hạnh kiểm</th>
      <th className="border border-slate-300 ">Chức Năng</th>
    
    </tr>
  </thead>
  <tbody id="tbodySinhVien"  className="text-center border border-slate-300"></tbody>
</table>



    </div>
  );
}
